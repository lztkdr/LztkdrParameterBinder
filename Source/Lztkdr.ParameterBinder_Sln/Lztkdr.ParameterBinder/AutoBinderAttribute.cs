﻿using System;
using Microsoft.AspNetCore.Mvc;

namespace Lztkdr.ParameterBinder
{

    /// <summary>
    /// 自动装配常见数据类型
    /// <list type="bullet">
    /// <item>
    /// 布尔
    /// <description>bool</description>
    /// </item>
    /// <item>
    /// 字符/字符串
    /// <description>char/string</description>
    /// </item>
    /// <item>
    /// 浮点型
    /// <description>float/double/decimal</description>
    /// </item>
    /// <item>
    /// 整型
    /// <description>byte/sbyte/short/ushort/int/uint/long/ulong</description>
    /// </item>
    /// <item>
    /// 枚举
    /// <description>Enum</description>
    /// </item>
    /// <item>
    /// 其他类型
    /// <description>JObject/JArray/Uri/Guid/DateTime/TimeSpan</description>
    /// </item>
    /// <item>
    /// 复合类型
    /// <description>List/Dictionary/Model</description>
    /// </item>
    /// </list>
    /// </summary>
    [AttributeUsage(AttributeTargets.Parameter, AllowMultiple = false, Inherited = false)]
    public class AutoBinderAttribute : ModelBinderAttribute
    {

        public AutoBinderAttribute()
             : this(AutoType.None, null)
        {

        }

        /// <summary>
        /// 自动绑定
        /// </summary>
        /// <param name="type">枚举取值方式</param>
        public AutoBinderAttribute(AutoType type)
            : this(type, null)
        {

        }

        /// <summary>
        /// 自动绑定
        /// </summary>
        /// <param name="type">取值方式</param>
        /// <param name="fieldName">字段 或 jsonPath</param>
        public AutoBinderAttribute(AutoType type, string fieldName)
            : base(typeof(AutoBinder))

        {
            this.Type = type;
            this.FieldName = fieldName;
        }

        /// <summary>
        /// 字段 或 jsonPath
        /// </summary>
        public string FieldName { get; set; }

        /// <summary>
        /// 取值方式
        /// </summary>
        public AutoType Type { get; set; } = AutoType.None;
    }

    /// <summary>
    /// 枚举取值方式
    /// </summary>
    public enum AutoType
    {
        /// <summary>
        /// 无设定(GET请求时，从GET中取值，其他方式则从Form或Body中取值)
        /// </summary>
        None,

        /// <summary>
        /// 从 请求体 中获取值
        /// </summary>
        Body,

        /// <summary>
        /// 从 URL 中获取值
        /// </summary>
        Query,

        /// <summary>
        /// 从 提交表单 中获取值
        /// </summary>
        Form,

        /// <summary>
        /// 从 HTTP 标头中获取值
        /// </summary>
        Header,

        /// <summary>
        /// 从 Cookie 中取值
        /// </summary>
        Cookie,

        /// <summary>
        /// 从 路由 中获取值
        /// </summary>
        Route
    }
}
