﻿using Microsoft.AspNetCore.Builder;

namespace Lztkdr.ParameterBinder
{
    public static class AutoBinderMiddlewareExtensions
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="appBuilder">使用请求参数自动装配器</param>
        /// <returns></returns>
        public static IApplicationBuilder UseAutoBinder(this IApplicationBuilder appBuilder)
        {
            return appBuilder.UseMiddleware<AutoBinderMiddleware>();
        }
    }
}
