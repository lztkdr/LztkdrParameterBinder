﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;

namespace Lztkdr.ParameterBinder
{
    public sealed class AutoBinderMiddleware
    {
        private readonly RequestDelegate _next;
        private ILogger<AutoBinderMiddleware> _logger;

        public AutoBinderMiddleware(RequestDelegate next, ILogger<AutoBinderMiddleware> logger)
        {
            this._next = next;
            this._logger = logger;
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            finally
            {
                AutoBinder.CacheParser.TryRemove(context.TraceIdentifier, out _);
            }
        }
    }
}
