﻿using Lztkdr.ParameterBinder;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lztkdr.ParameterBinder.Test.Controllers
{

    [ApiController]
    [Route("[controller]/[action]")]
    public class HomeController : ControllerBase
    {
        /// <summary>
        /// 网站主页
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult Index()
        {
            Response.Cookies.Append("username2", "admin", new Microsoft.AspNetCore.Http.CookieOptions() { Expires = DateTimeOffset.UtcNow.AddDays(10) });
            return Ok();
        }


        [HttpGet]
        public IActionResult Test(
            [FromQuery] string sid,
            [AutoBinder] int id,
            [AutoBinder] string username,
            [AutoBinder] DateTime dt,
            [AutoBinder] string[] names,
            [AutoBinder] Gender gender,
            [AutoBinder] List<Gender> genders,
            [AutoBinder] Uri uri,
            [AutoBinder] Guid newId,
            [AutoBinder] TimeSpan ts,
            [AutoBinder(AutoType.Query)] LogInfo info,
            [AutoBinder(AutoType.Header)] Dictionary<string, string> header,
            [AutoBinder(AutoType.Header, "User-Agent")] string userAgent,
            [AutoBinder(AutoType.Route)] Dictionary<string, string> route,
            [AutoBinder(AutoType.Route, "controller")] string controller,
            [AutoBinder(AutoType.Route)] RouteInfo routeInfo,
            [AutoBinder(AutoType.Cookie)] string username2,
            [AutoBinder(AutoType.Cookie)] Dictionary<string, string> cookie,
            [AutoBinder] string defValue = "123",
            [AutoBinder] int pagesize = 100
            )
        {

            /*
           
            http://localhost:6200/Home/Test?sid=abc&id=111&userName=aaa&password=bbb&gender=1&uri=https://localhost:44342/swagger/index.html&newId=3fa85f64-5717-4562-b3fc-2c963f66afa6&names=小强&names=小明&ts=1:10:10&genders=2&genders=2&defValue=AAA&pagesize=150

            */
            return Ok();
        }


        [HttpPost]
        public IActionResult TestPost(
            [AutoBinder(AutoType.Query)] string sid,
            [AutoBinder(AutoType.Body, "childgroups[0].fields")] List<fieldInfo> lstFld
            )
        {

            /*
             

           {
            "showtitle": true,
            "title": "基本信息",
            "disabled": false,
            "childgroups": [
                {
                    "showtitle": false,
                    "showfilterstring": "",
                    "fields": [
                        {
                            "fieldtype": "text",
                            "name": "tbbh",
                            "tablename": "main",
                            "itemsize": "H50F25",
                            "alias": "地块编号",
                            "clearcascadefilter": false
                        },
                        {
                            "fieldtype": "text",
                            "name": "dkbh",
                            "tablename": "main",
                            "itemsize": "H50F25",
                            "alias": "地块标识",
                            "clearcascadefilter": false
                        }
                    ]
                }
            ]
        }

             
             */

            return Ok();
        }


    }


    public class fieldInfo
    {
        public string fieldtype { get; set; }
        public string name { get; set; }
        public string tablename { get; set; }
        public string itemsize { get; set; }
        public string alias { get; set; }
        public bool clearcascadefilter { get; set; }
    }


    public class RouteInfo
    {
        public string controller { get; set; }
        public string action { get; set; }
    }

    public enum Gender
    {
        保密 = 0,
        男 = 1,
        女 = 2,
    }
    public class LogInfo
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }

        public Gender Gender { get; set; }
        public Uri URI { get; set; }
        public Guid NewId { get; set; }

        public TimeSpan TS { get; set; }

        public List<Gender> Genders { get; set; }
    }
}
