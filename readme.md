﻿

# Lztkdr.ParameterBinder

### 一个针对 Asp.NET Core MVC/WebAPI 的自动参数绑值中间件。

##  支持的绑定方式

- [x] None
- [x] Body
- [x] Query
- [x] Form
- [x] Header
- [x] Cookie
- [x] Route

## 使用指南

### 1.  安装

``` shell

Install-Package Lztkdr.ParameterBinder

```
### 2. **Startup.cs** 中启用

- 类库引用
``` cs
using Lztkdr.ParameterBinder;
```
-  放置位置
``` cs
app.UseRouting();
//使用请求参数自动装配器
app.UseAutoBinder();
```
### 3. 使用特性 **[AutoBinder]**

- 使用示例

``` cs
 [HttpGet]
 public IActionResult Test(
     [FromQuery] string sid,
     [AutoBinder] int id,
     [AutoBinder] string username,
     [AutoBinder] DateTime dt,
     [AutoBinder] string[] names,
     [AutoBinder] Gender gender,
     [AutoBinder] List<Gender> genders,
     [AutoBinder] Uri uri,
     [AutoBinder] Guid newId,
     [AutoBinder] TimeSpan ts,
     [AutoBinder(AutoType.Query)] LogInfo info,
     [AutoBinder(AutoType.Header)] Dictionary<string, string> header,
     [AutoBinder(AutoType.Header, "User-Agent")] string userAgent,
     [AutoBinder(AutoType.Route)] Dictionary<string, string> route,
     [AutoBinder(AutoType.Route, "controller")] string controller,
     [AutoBinder(AutoType.Route)] RouteInfo routeInfo,
     [AutoBinder(AutoType.Cookie)] string username2,
     [AutoBinder(AutoType.Cookie)] Dictionary<string, string> cookie,
     [AutoBinder] string defValue = "123",
     [AutoBinder] int pagesize = 100
     )
 {
     return Ok();
 }


 [HttpPost]
 public IActionResult TestPost(
     [AutoBinder(AutoType.Query)] string sid,
     [AutoBinder(AutoType.Body, "childgroups[0].fields")] List<fieldInfo> lstFld
     )
 {
    return Ok();
 }
```

## 贡献代码

`Lztkdr.ParameterBinder` 遵循 `Apache-2.0` 开源协议，欢迎大家一起完善，提交 `PR` 或 `Issue`。

